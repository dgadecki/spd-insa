import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class TasksList {

	List<Proces> tasksList;
	
	public TasksList(){
		tasksList = new ArrayList<Proces>();
	}
	
	public void sortByR(){
		for(int i = 0; i < tasksList.size(); i++)
			for(int j = 0; j < tasksList.size() - 1; j++)
				if(tasksList.get(j).time < tasksList.get(j+1).time)
					Collections.swap(tasksList, j, j+1);
	}
	
	public void writeList(){
		for(Proces p : tasksList)
			System.out.println(p.id + " " + p.machine + " " + p.time);
	}
	
	public boolean isContainsId(int id){
		for(int i = 0; i < tasksList.size(); i++)
			if(tasksList.get(i).id == id)
				return true;
		
		return false;
	}
	
	
}
