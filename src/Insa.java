import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Insa {
	
	TasksList operationList = new TasksList();
	TasksList sortedByRList = new TasksList();
	TasksList topologicalList = new TasksList();
	List<Integer> CmaxTimes = new ArrayList<Integer>();
	List<Integer> minCmaxTimes = new ArrayList<Integer>();
	String instanceName;
	int machines;
	int tasks;
	int Cmax;
	int minCmax;
	int [][] insaMatrix;
	int [][] gantDiagram;
	

	@SuppressWarnings("unchecked")
	public void loadFile(String fileName) throws FileNotFoundException{
		Scanner file = new Scanner(new File(fileName));
		PrintWriter gantFile = new PrintWriter("PermutationOut.txt");
		
		int i = 1;
		int processes;
		long start, stop;
		
		while(file.hasNext())
		{
			i = 1;
			instanceName = file.next();
			tasks = file.nextInt();
			machines = file.nextInt();
			processes = tasks*machines;
			while(processes-- > 0)
				operationList.tasksList.add(new Proces(i++, file.nextInt(), file.nextInt()));
			
			sortedByRList.tasksList = (List<Proces>)((ArrayList<Proces>) operationList.tasksList).clone();
			System.out.print("Wykonuje instancje: " + instanceName + "  ");
			start = System.currentTimeMillis();
			runINSA();
			stop = System.currentTimeMillis();
			System.out.println("Czas: " + (stop-start) + " ms");
			savePermutation(gantFile, instanceName);
			CmaxTimes.add(Cmax);
			minCmaxTimes.add(minCmax);
			operationList.tasksList.clear();
		
		}
		file.close();
		gantFile.close();
	}
	
	public void createINSAMatrix(){
		insaMatrix = new int [machines*tasks + 1][10];
		
		for(int i = 0; i < insaMatrix.length - 1; i++){
			insaMatrix[i+1][Column.lp.ordinal()] = operationList.tasksList.get(i).id;
			insaMatrix[i+1][Column.machine.ordinal()] = operationList.tasksList.get(i).machine;
			insaMatrix[i+1][Column.pTime.ordinal()] = operationList.tasksList.get(i).time;
			
			if(i%machines != 0 && i-1 >= 0)
				insaMatrix[i+1][Column.PO.ordinal()] = operationList.tasksList.get(i-1).id;
			
			if(i%machines != (machines - 1) && i+1 < insaMatrix.length)
				insaMatrix[i+1][Column.NO.ordinal()] = operationList.tasksList.get(i+1).id;
		}
	}
	
	public void createGantDiagram(){
		gantDiagram = new int [machines][tasks];
	}
	
	public void calculateLP(){
		for(int i = 0; i < insaMatrix.length; i++){
			if(insaMatrix[i][Column.PO.ordinal()] != 0 && insaMatrix[i][Column.PM.ordinal()] != 0)
				insaMatrix[i][Column.LP.ordinal()] = 2;
			else if(insaMatrix[i][Column.PO.ordinal()] == 0 && insaMatrix[i][Column.PM.ordinal()] == 0)
				insaMatrix[i][Column.LP.ordinal()] = 0;
			else
				insaMatrix[i][Column.LP.ordinal()] = 1;
		}
	}
	
	public void writeINSAMatrix(){
		for(int i  = 0; i < insaMatrix.length; i++){
			for(int j = 0; j < insaMatrix[i].length; j++)
				System.out.print(insaMatrix[i][j]  +" " );
			
			System.out.print("\n");
		}	
	}
	
	public void writeGantDiagram(){
		for(int i  = 0; i < gantDiagram.length; i++){
			for(int j = 0; j < gantDiagram[i].length; j++)
				System.out.print(gantDiagram[i][j] + " " );
			
			System.out.print("\n");
		}
	}

	public boolean checkLPColumn(){
		for(int i = 1; i <= tasks*machines; i++)
			if(insaMatrix[i][Column.LP.ordinal()] != 0)
				return false; 
		
		return true;
	}
	
	public void sortTopological(){
		topologicalList.tasksList.clear();
		int tmpId;
		
		for(int i = 1; i <= machines*tasks; i++)
			if(insaMatrix[i][Column.LP.ordinal()] == 0)
				topologicalList.tasksList.add(operationList.tasksList.get(i-1));
		
		for(int i = 0; i < topologicalList.tasksList.size(); i++){
			tmpId = insaMatrix[topologicalList.tasksList.get(i).id][Column.NO.ordinal()];
			insaMatrix[tmpId][Column.LP.ordinal()] -= 1;
			if(insaMatrix[insaMatrix[topologicalList.tasksList.get(i).id][Column.NO.ordinal()]][Column.LP.ordinal()] == 0)
				topologicalList.tasksList.add(operationList.tasksList.get(insaMatrix[topologicalList.tasksList.get(i).id][Column.NO.ordinal()] - 1));
			
			tmpId = insaMatrix[topologicalList.tasksList.get(i).id][Column.NM.ordinal()];
			insaMatrix[tmpId][Column.LP.ordinal()] -= 1;
			if(insaMatrix[insaMatrix[topologicalList.tasksList.get(i).id][Column.NM.ordinal()]][Column.LP.ordinal()] == 0)
				topologicalList.tasksList.add(operationList.tasksList.get(insaMatrix[topologicalList.tasksList.get(i).id][Column.NM.ordinal()] - 1));
		}
			
	}

	public void calculateRTime(){
		int maxRPO = 0;
		int maxRPM = 0;
		for(int i = 0; i < tasks*machines; i++){
			maxRPO = insaMatrix[insaMatrix[topologicalList.tasksList.get(i).id][Column.PO.ordinal()]][Column.R.ordinal()];
			maxRPM = insaMatrix[insaMatrix[topologicalList.tasksList.get(i).id][Column.PM.ordinal()]][Column.R.ordinal()];
			insaMatrix[topologicalList.tasksList.get(i).id][Column.R.ordinal()] = insaMatrix[topologicalList.tasksList.get(i).id][Column.pTime.ordinal()] + Math.max(maxRPO, maxRPM);
		}
	}
	
	public void calculateQTime(){
		int maxQNO = 0;
		int maxQNP = 0;
		for(int i = tasks*machines - 1; i >= 0; i--){
			maxQNO = insaMatrix[insaMatrix[topologicalList.tasksList.get(i).id][Column.NO.ordinal()]][Column.Q.ordinal()];
			maxQNP = insaMatrix[insaMatrix[topologicalList.tasksList.get(i).id][Column.NM.ordinal()]][Column.Q.ordinal()];
			insaMatrix[topologicalList.tasksList.get(i).id][Column.Q.ordinal()] = insaMatrix[topologicalList.tasksList.get(i).id ][Column.pTime.ordinal()] + Math.max(maxQNO, maxQNP);
		}
	}
	
	public void addTaskToGantDiagram(int machine, int id, int index){
		
		if(gantDiagram[machine - 1][index] == 0)
			gantDiagram[machine - 1][index] = id;
		else{
			for(int i = checkMachine(machine); i > index; i--)
				gantDiagram[machine - 1][i] = gantDiagram[machine - 1][i-1];
			
			gantDiagram[machine - 1][index] = id;
		}
	}
	
	public int checkMachine(int machine){
		for(int i = 0; i < tasks; i++)
			if(gantDiagram[machine - 1][i] == 0)
				return i;
		
		return tasks;
	}
	
	public void addNM(){
		for(int i = 0; i < gantDiagram.length; i++)
			for(int j = 0; j < gantDiagram[i].length - 1; j++){
				if(gantDiagram[i][j+1] != 0)
					insaMatrix[gantDiagram[i][j]][Column.NM.ordinal()] = gantDiagram[i][j+1];
				else
					break;
			}
	}
	
	public void addPM(){
		for(int i = 0; i < gantDiagram.length; i++)
			for(int j = 1; j < gantDiagram[i].length;j++){
				if(gantDiagram[i][j-1] != 0 && gantDiagram[i][j] != 0)
					insaMatrix[gantDiagram[i][j]][Column.PM.ordinal()] = gantDiagram[i][j-1];
			}
	}
	
	public void addTaskToMachine(){
		
		int min;
		int minTmp;
		int index;
		int tasksOnMachine;
		for(int i = 0; i < tasks*machines; i++){
			if(gantDiagram[sortedByRList.tasksList.get(0).machine - 1][0] == 0){
				addTaskToGantDiagram(sortedByRList.tasksList.get(0).machine, sortedByRList.tasksList.get(0).id , 0);
				sortedByRList.tasksList.remove(0);
			}
			else{
				min = Integer.MAX_VALUE;
				minTmp = Integer.MAX_VALUE;
				index = 0;
				tasksOnMachine = checkMachine(sortedByRList.tasksList.get(0).machine);
				for(int j = 0; j <= tasksOnMachine; j++){
					if(j == 0){
						minTmp = Math.max(insaMatrix[insaMatrix[sortedByRList.tasksList.get(0).id][Column.PO.ordinal()]][Column.R.ordinal()], 0) + 
							  Math.max(insaMatrix[insaMatrix[sortedByRList.tasksList.get(0).id][Column.NO.ordinal()]][Column.Q.ordinal()], 
									   insaMatrix[gantDiagram[sortedByRList.tasksList.get(0).machine - 1][j]][Column.Q.ordinal()]);
						
						if(minTmp < min)
							min = minTmp;
						
					}
					else if(j == tasksOnMachine){
						minTmp = Math.max(insaMatrix[insaMatrix[sortedByRList.tasksList.get(0).id][Column.PO.ordinal()]][Column.R.ordinal()], 
								          insaMatrix[gantDiagram[sortedByRList.tasksList.get(0).machine - 1][j-1]][Column.R.ordinal()]) + 
								 Math.max(insaMatrix[insaMatrix[sortedByRList.tasksList.get(0).id][Column.NO.ordinal()]][Column.Q.ordinal()], 0);
											
						if(minTmp < min){
							min = minTmp;
							index = j;
						}
							
					}
					else{
						minTmp = Math.max(insaMatrix[insaMatrix[sortedByRList.tasksList.get(0).id][Column.PO.ordinal()]][Column.R.ordinal()], 
										  insaMatrix[gantDiagram[sortedByRList.tasksList.get(0).machine - 1][j-1]][Column.R.ordinal()]) + 
								 Math.max(insaMatrix[insaMatrix[sortedByRList.tasksList.get(0).id][Column.NO.ordinal()]][Column.Q.ordinal()],
										  insaMatrix[gantDiagram[sortedByRList.tasksList.get(0).machine - 1][j]][Column.Q.ordinal()]);
						
						if(minTmp < min){
							min = minTmp;
							index = j;
						}				  
					}
				}
				addTaskToGantDiagram(sortedByRList.tasksList.get(0).machine, sortedByRList.tasksList.get(0).id , index);
				sortedByRList.tasksList.remove(0);
				addNM();
				addPM();
				calculateLP();
				sortTopological();
				calculateRTime();
				calculateQTime();
			}
				
		}
	}
	
	public void findCmax(){
		Cmax = Integer.MIN_VALUE;
		for(int i = 1; i <= tasks*machines; i++)
			if(insaMatrix[i][Column.R.ordinal()] > Cmax)
				Cmax = insaMatrix[i][Column.R.ordinal()];
	}
	
	//Popraw tutaj liczenie Cmax
	public void findBetterPermutation(int machine, int indexOne, int indexTwo){
		int tmp = 0;
		tmp = gantDiagram[machine][indexOne];
		gantDiagram[machine][indexOne] = gantDiagram[machine][indexTwo];
		gantDiagram[machine][indexTwo] = tmp;
		
		changeTasks(machine, indexOne, indexTwo);
		
		calculateRTime();
		findCmax();
		
		if(Cmax >= minCmax){
			tmp = gantDiagram[machine][indexOne];
			gantDiagram[machine][indexOne] = gantDiagram[machine][indexTwo];
			gantDiagram[machine][indexTwo] = tmp;
			changeTasks(machine, indexOne, indexTwo);
			calculateRTime();
			findCmax();
		}
		
		if(Cmax < minCmax)
			minCmax = Cmax;
	}

	public void changeTasks(int machine, int indexOne, int indexTwo) {
		insaMatrix[gantDiagram[machine][indexOne - 1]][Column.NM.ordinal()] = gantDiagram[machine][indexOne];	
		insaMatrix[gantDiagram[machine][indexTwo + 1]][Column.PM.ordinal()] = gantDiagram[machine][indexTwo];
		
		insaMatrix[gantDiagram[machine][indexOne + 1]][Column.PM.ordinal()] = gantDiagram[machine][indexOne];
		insaMatrix[gantDiagram[machine][indexTwo - 1]][Column.NM.ordinal()] = gantDiagram[machine][indexTwo];
		
		insaMatrix[gantDiagram[machine][indexOne]][Column.PM.ordinal()] = gantDiagram[machine][indexOne - 1];
		insaMatrix[gantDiagram[machine][indexOne]][Column.NM.ordinal()] = gantDiagram[machine][indexOne + 1];
		
		insaMatrix[gantDiagram[machine][indexTwo]][Column.PM.ordinal()] = gantDiagram[machine][indexTwo - 1];
		insaMatrix[gantDiagram[machine][indexTwo]][Column.NM.ordinal()] = gantDiagram[machine][indexTwo + 1];
	}
	
	public void runINSA(){
		createINSAMatrix();
		createGantDiagram();
		calculateLP();
		sortTopological();
		calculateRTime();
		calculateQTime();
		sortedByRList.sortByR();
		addTaskToMachine();
		findCmax();
		Random rand = new Random();
		minCmax = Cmax;
		int i = 2000;
		while(i > 0){
			findBetterPermutation(rand.nextInt(machines),rand.nextInt(tasks - 2) + 1,rand.nextInt(tasks - 2) + 1);
			i--;
		}
		System.out.println("Minimum: " + minCmax);
	}
	
	public void saveCmaxTimes(String fileName) throws FileNotFoundException{
		PrintWriter file = new PrintWriter(fileName);
		for(int i = 0; i < CmaxTimes.size(); i++)
			file.println("tr0" + (i+1) + ":  " + CmaxTimes.get(i));
		
		file.close();
	}
	
	public void savePermutation(PrintWriter p, String insName){
		p.println(insName);
		for(int i = 0; i < gantDiagram.length; i++){
			for(int j = 0; j <gantDiagram[i].length; j++)
				p.print(gantDiagram[i][j] + " ");
			
			p.println("");
		}
	}
	
	public int porownanie(){
		
		int i = 0;
		
		for(int j = 0; j < CmaxTimes.size(); j++)
			if(CmaxTimes.get(j) > minCmaxTimes.get(j))
				i++;
		
		return i;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		Insa i = new Insa();
		long start = System.currentTimeMillis();
		i.loadFile("bench_js.txt");
		long stop = System.currentTimeMillis();
		System.out.println(i.porownanie());
	}



}
